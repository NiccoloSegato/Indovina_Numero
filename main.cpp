#include <iostream>
#include <cstdlib>
#include <ctime>
#define S -1

using namespace std;

int main () {

    int n_segreto, numero, i = 0;

    srand (time(NULL));

    n_segreto = rand () %100;

    cout << "-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-" << endl << endl;

    cout << "   ***  Indovina il numero pensato dal computer  ***" << endl << endl;

    cout << "-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-" << endl << endl;

    cout << "               ***     REGOLE      ***" << endl << endl;

    cout << "REGOLA 1: Il numero e' compreso tra 0 e 99" << endl;

    cout << "REGOLA 2: Quando vuoi uscire dal programma premi -1" << endl << endl;

    cout << "               ***   FINE REGOLE   ***" << endl << endl;

    cout << "-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-" << endl << endl;

    cout << "                  Pronto? Cominciamo!" << endl << endl;

    cout << "-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-" << endl << endl;

    cout << "   *** Il numero e' stato pensato, prova ad indovinarlo! ***" << endl << endl;

    do {

        cout << "Tentativo " << i + 1 << ": ";

        cin >> numero;

        cout << endl;

        if (numero > 99) {

            while (numero > 99) {

                cout << "Numero non valido, reinseriscilo." << endl;

                cout << "Fai attenzione che sia un numero minore o uguale a 99." << endl << endl;

                cout << "Tentativo " << i + 1 << ": ";

                cin >> numero;

                cout << endl;

                if (n_segreto < numero && numero != -1 && numero <= 99) {

                    cout << "Il numero segreto e' piu' piccolo, diminuisci!" << endl << endl;

                }

                else if (n_segreto > numero && numero != -1 && numero <= 99){

                    cout << "Il numero segreto e' piu' grande, aumenta!" << endl << endl;

                }

            }

            i++;

        }

        else if (n_segreto < numero && numero != -1) {

            cout << "Il numero segreto e' piu' piccolo, diminuisci!" << endl << endl;

        }

        else if (n_segreto > numero && numero != -1){

            cout << "Il numero segreto e' piu' grande, aumenta!" << endl << endl;

        }

        i++;

    } while (n_segreto != numero && numero != S);

    if (numero != -1) {

        cout << "-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-" << endl << endl;

        cout << "   *** Ben fatto! Hai indovinato il numero segreto '" << n_segreto << "' in " << i << " tentativi! ***" << endl << endl;

        cout << "-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-" << endl << endl;

        cout << "               *** PROGRAMMA TERMINATO ***" << endl << endl;

    }

    else if (numero == -1) {

        cout << endl;

        cout << "-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-" << endl << endl;

        cout << "               *** PROGRAMMA TERMINATO ***" << endl << endl;

    }

    system("pause");
    return 0;

}

